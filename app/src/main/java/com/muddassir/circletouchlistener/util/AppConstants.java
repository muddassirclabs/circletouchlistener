package com.muddassir.circletouchlistener.util;

/**
 * Created by Muddassir on 11-Feb-15.
 */
public interface AppConstants {
    String INSIDE_CIRCLE = "Touch is inside the circle ";
    String OUTSIDE_CIRCLE = "Touch is outside the circles ";
    String ON_THE_CIRCLE = "Touch is on the circle ";

    int ONE = 0;
    int TWO = 1;
    int THREE = 2;
    int FOUR = 3;
    int FIVE = 4;
    int SIX = 5;
    int MAX_SIZE = 6;
    int MAX_COLUMN = 4;
    String MAX_COLUMN_ROW_REACHED = "column or row count must be less than " + (MAX_COLUMN + 1);
    int MAX_ROW = 4;
    int MAX_CIRCLE_COUNT = MAX_COLUMN * MAX_ROW;
    boolean RED = true;
    boolean BLACK = false;
    String NO_COLUMN_ERROR = "Please Enter number of columns first!";
    String NO_ROW_ERROR = "Please Enter number of rows first!";
    String NULL = "";
}

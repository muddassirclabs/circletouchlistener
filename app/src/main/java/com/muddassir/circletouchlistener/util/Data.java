package com.muddassir.circletouchlistener.util;

import static com.muddassir.circletouchlistener.util.AppConstants.MAX_COLUMN;
import static com.muddassir.circletouchlistener.util.AppConstants.MAX_ROW;

public class Data {
    public static double[] centerX = new double[MAX_COLUMN * MAX_ROW];
    public static double[] centerY = new double[MAX_COLUMN * MAX_ROW];
}

package com.muddassir.circletouchlistener.activities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import com.muddassir.circletouchlistener.R;
import com.muddassir.circletouchlistener.util.Data;

import java.util.ArrayList;
import java.util.List;

import static com.muddassir.circletouchlistener.util.AppConstants.MAX_COLUMN;
import static com.muddassir.circletouchlistener.util.AppConstants.MAX_ROW;


public class CircleAdapter extends BaseAdapter {
    List<Button> circleNumber;
    Context context;
    View[] view = new View[MAX_COLUMN * MAX_ROW];
    Button current;

    public CircleAdapter(Context ctx) {
        circleNumber = new ArrayList<>();
        this.context = ctx;
    }


    @Override
    public int getCount() {
        return circleNumber.size();
    }

    @Override
    public Object getItem(int position) {
        current = (Button) view[position].findViewById(R.id.circle_button);
        //return circleNumber.get(position);
        return current;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(context.LAYOUT_INFLATER_SERVICE);

        view[position] = inflater.inflate(R.layout.circle, null);
        /*Button current = (Button) view[position].findViewById(R.id.circle_button);
        Data.centerX[position] = current.getX() + current.getWidth() / 2;
        Data.centerY[position] = current.getY() + current.getHeight() / 2;*/
        return view[position];
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

    public void setButton(Button newButton) {
        current = newButton;
    }

    public void calculateCenters() {
        for (int i = 0; i < getCount(); i++) {
            View circleButton = view[i];
            Data.centerX[i] = circleButton.getX() + circleButton.getWidth() / 2;
            Data.centerY[i] = circleButton.getY() + circleButton.getHeight() / 2;
        }
    }
}


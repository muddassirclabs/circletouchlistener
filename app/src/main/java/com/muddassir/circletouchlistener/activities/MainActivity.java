package com.muddassir.circletouchlistener.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.muddassir.circletouchlistener.R;

import static com.muddassir.circletouchlistener.util.AppConstants.INSIDE_CIRCLE;
import static com.muddassir.circletouchlistener.util.AppConstants.ON_THE_CIRCLE;
import static com.muddassir.circletouchlistener.util.AppConstants.OUTSIDE_CIRCLE;


public class MainActivity extends Activity implements View.OnTouchListener {
    RelativeLayout relativeLayout;
    double xCoordinate, yCoordinate, centerXCoordinate, centerYCoordinate, radius, distance;
    TextView output;
    Button circleButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        relativeLayout = (RelativeLayout) findViewById(R.id.touch_layout);
        relativeLayout.setOnTouchListener(this);
        output = (TextView) findViewById(R.id.output);
        circleButton = (Button) findViewById(R.id.circle_button);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                getXYCoordinate(event);
                showPosition();
                break;
            case MotionEvent.ACTION_MOVE:
                getXYCoordinate(event);
                showPosition();
                break;
            case MotionEvent.ACTION_UP:
                output.setText(R.string.touch_circle);
                break;
        }
        return true;
    }

    private void showPosition() {
        radius = calculateRadius();
        calculateCanterCoordinate();
        distance = calculateDistance();
        if (radius > distance) {
            output.setText(INSIDE_CIRCLE);
        } else if (radius < distance) {
            output.setText(OUTSIDE_CIRCLE);
        } else {
            output.setText(ON_THE_CIRCLE);
        }
    }

    private double calculateDistance() {
        double xDifference = centerXCoordinate - xCoordinate;
        double yDifference = centerYCoordinate - yCoordinate;
        return Math.sqrt(Math.pow(xDifference, 2) + Math.pow(yDifference, 2));
    }

    private void calculateCanterCoordinate() {
        centerYCoordinate = relativeLayout.getHeight() / 2;
        centerXCoordinate = relativeLayout.getWidth() / 2;
    }

    private double calculateRadius() {
        return (circleButton.getWidth() / 2);
    }

    private void getXYCoordinate(MotionEvent event) {
        xCoordinate = event.getX();
        yCoordinate = event.getY();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.next_button:
                Intent intent = new Intent(this, SixCircle.class);
                startActivity(intent);
                break;
        }
    }
}

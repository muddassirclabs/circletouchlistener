package com.muddassir.circletouchlistener.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.muddassir.circletouchlistener.R;
import com.muddassir.circletouchlistener.util.Data;

import static com.muddassir.circletouchlistener.util.AppConstants.BLACK;
import static com.muddassir.circletouchlistener.util.AppConstants.INSIDE_CIRCLE;
import static com.muddassir.circletouchlistener.util.AppConstants.MAX_CIRCLE_COUNT;
import static com.muddassir.circletouchlistener.util.AppConstants.MAX_COLUMN;
import static com.muddassir.circletouchlistener.util.AppConstants.MAX_COLUMN_ROW_REACHED;
import static com.muddassir.circletouchlistener.util.AppConstants.MAX_ROW;
import static com.muddassir.circletouchlistener.util.AppConstants.NO_COLUMN_ERROR;
import static com.muddassir.circletouchlistener.util.AppConstants.NO_ROW_ERROR;
import static com.muddassir.circletouchlistener.util.AppConstants.NULL;
import static com.muddassir.circletouchlistener.util.AppConstants.ON_THE_CIRCLE;
import static com.muddassir.circletouchlistener.util.AppConstants.OUTSIDE_CIRCLE;
import static com.muddassir.circletouchlistener.util.AppConstants.RED;

public class UserDefineCircles extends Activity implements View.OnTouchListener {
    CircleAdapter adapter;
    GridView gridView;
    EditText row, column;
    int numRows, numColumns;
    RelativeLayout relativeLayout;
    double xCoordinate, yCoordinate, radius, finalDistance;
    TextView output;
    boolean[] flag;
    boolean touched = false;
    int currentCircle = MAX_CIRCLE_COUNT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_define_circles);
        initComponents();
    }

    private void initComponents() {
        gridView = (GridView) findViewById(R.id.grid_view);
        adapter = new CircleAdapter(this);
        output = (TextView) findViewById(R.id.output);
        gridView.setAdapter(adapter);
        row = (EditText) findViewById(R.id.row_count);
        column = (EditText) findViewById(R.id.column_count);
        relativeLayout = (RelativeLayout) findViewById(R.id.touch_layout);
        gridView.setOnTouchListener(this);
        //relativeLayout.setOnTouchListener(this);

        flag = new boolean[MAX_COLUMN * MAX_ROW];
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int circleNumber;
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (!adapter.isEmpty()) {
                    getXYCoordinate(event);
                    if (touched) {
                        radius = calculateRadius();
                        adapter.calculateCenters();
                        touched = false;
                    }
                    circleNumber = getCircleNumber();
                    showPosition(circleNumber);
                    changeButtonColor(circleNumber);
                    gridView.requestFocus();
                }
                break;
            case MotionEvent.ACTION_MOVE:
                getXYCoordinate(event);
                if (touched) {
                    radius = calculateRadius();
                    adapter.calculateCenters();
                    touched = false;
                }
                circleNumber = getCircleNumber();
                showPosition(circleNumber);
                if (circleNumber != currentCircle) {
                    currentCircle = changeButtonColor(circleNumber);
                    gridView.requestFocus();
                }
                break;
            case MotionEvent.ACTION_UP:
                output.setText(R.string.touch_circle);
                break;
        }
        return true;
    }

    private int changeButtonColor(int circleNumber) {
        if (circleNumber != MAX_CIRCLE_COUNT) {
            Button currentB = (Button) adapter.getItem(circleNumber);
            if (flag[circleNumber] == RED) {
                currentB.setBackgroundResource(R.drawable.circle_button);
                adapter.setButton(currentB);
                flag[circleNumber] = BLACK;
            } else {
                currentB.setBackgroundResource(R.drawable.touched_circle);
                adapter.setButton(currentB);
                flag[circleNumber] = RED;
            }
        }
        return circleNumber;
    }

    private void showPosition(int currentCircle) {
        if (radius > finalDistance) {
            output.setText(INSIDE_CIRCLE + (currentCircle + 1));
        } else if (radius < finalDistance) {
            output.setText(OUTSIDE_CIRCLE);
        } else {
            output.setText(ON_THE_CIRCLE + (currentCircle + 1));
        }
    }

    private int getCircleNumber() {
        int number = MAX_CIRCLE_COUNT;
        double distance = Double.MAX_VALUE;
        for (int i = 0; i < adapter.getCount(); i++) {
            double tempDistance = calculateDistance(Data.centerX[i], Data.centerY[i]);
            if (distance > tempDistance && tempDistance <= radius) {
                distance = tempDistance;
                number = i;
                break;
            }
        }
        finalDistance = distance;
        return number;
    }

    private double calculateDistance(double centerXCoordinate, double centerYCoordinate) {
        double xDifference = centerXCoordinate - xCoordinate;
        double yDifference = centerYCoordinate - yCoordinate;
        return Math.sqrt(Math.pow(xDifference, 2) + Math.pow(yDifference, 2));
    }

    private double calculateRadius() {
        Button currentB = (Button) adapter.getItem(0);
        return (currentB.getWidth() / 2);
    }

    private void getXYCoordinate(MotionEvent event) {
        xCoordinate = event.getX();
        yCoordinate = event.getY();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.submit:
                createCircles();
                break;
        }
    }

    private void createCircles() {
        if (!row.getText().toString().equals(NULL)) {
            numRows = Integer.parseInt(row.getText().toString());
            if (!column.getText().toString().equals(NULL)) {
                numColumns = Integer.parseInt(column.getText().toString());
                if (numRows > MAX_ROW || numColumns > MAX_COLUMN) {
                    Toast.makeText(this, MAX_COLUMN_ROW_REACHED, Toast.LENGTH_SHORT).show();
                } else {
                    gridView.setNumColumns(numColumns);
                    int numCircles = numColumns * numRows;
                    if (adapter.getCount() > 0) {
                        adapter.circleNumber.clear();
                    }
                    for (int i = 0; i < numCircles; i++) {
                        Button newB = new Button(this);
                        newB.setBackgroundResource(R.drawable.circle_button);
                        adapter.circleNumber.add(newB);
                        flag[i] = BLACK;
                    }
                    adapter.notifyDataSetChanged();
                    touched = true;
                }
            } else {
                Toast.makeText(this, NO_COLUMN_ERROR, Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, NO_ROW_ERROR, Toast.LENGTH_SHORT).show();
        }
    }
}

package com.muddassir.circletouchlistener.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.muddassir.circletouchlistener.R;

import static com.muddassir.circletouchlistener.util.AppConstants.BLACK;
import static com.muddassir.circletouchlistener.util.AppConstants.FIVE;
import static com.muddassir.circletouchlistener.util.AppConstants.FOUR;
import static com.muddassir.circletouchlistener.util.AppConstants.INSIDE_CIRCLE;
import static com.muddassir.circletouchlistener.util.AppConstants.MAX_SIZE;
import static com.muddassir.circletouchlistener.util.AppConstants.ONE;
import static com.muddassir.circletouchlistener.util.AppConstants.ON_THE_CIRCLE;
import static com.muddassir.circletouchlistener.util.AppConstants.OUTSIDE_CIRCLE;
import static com.muddassir.circletouchlistener.util.AppConstants.RED;
import static com.muddassir.circletouchlistener.util.AppConstants.SIX;
import static com.muddassir.circletouchlistener.util.AppConstants.THREE;
import static com.muddassir.circletouchlistener.util.AppConstants.TWO;

public class SixCircle extends Activity implements View.OnTouchListener {
    RelativeLayout relativeLayout;
    double xCoordinate, yCoordinate, radius, finalDistance;
    double[] centerX, centerY;
    TextView output;
    Button[] circles;
    boolean[] flag;
    int currentCircle = MAX_SIZE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_six_circle);

        initComponents();
    }

    private void calculateCenters() {
        centerX[ONE] = circles[ONE].getX() + circles[ONE].getWidth() / 2;
        centerY[ONE] = circles[ONE].getY() + circles[ONE].getHeight() / 2;

        centerX[TWO] = circles[TWO].getX() + circles[TWO].getWidth() / 2;
        centerY[TWO] = circles[TWO].getY() + circles[TWO].getHeight() / 2;

        centerX[THREE] = circles[THREE].getX() + circles[THREE].getWidth() / 2;
        centerY[THREE] = circles[THREE].getY() + circles[THREE].getHeight() / 2;

        centerX[FOUR] = circles[FOUR].getX() + circles[FOUR].getWidth() / 2;
        centerY[FOUR] = circles[FOUR].getY() + circles[FOUR].getHeight() / 2;

        centerX[FIVE] = circles[FIVE].getX() + circles[FIVE].getWidth() / 2;
        centerY[FIVE] = circles[FIVE].getY() + circles[FIVE].getHeight() / 2;

        centerX[SIX] = circles[SIX].getX() + circles[SIX].getWidth() / 2;
        centerY[SIX] = circles[SIX].getY() + circles[SIX].getHeight() / 2;
    }

    private void initComponents() {
        relativeLayout = (RelativeLayout) findViewById(R.id.touch_layout);
        relativeLayout.setOnTouchListener(this);
        output = (TextView) findViewById(R.id.output);
        centerX = new double[MAX_SIZE];
        centerY = new double[MAX_SIZE];

        circles = new Button[MAX_SIZE];
        circles[ONE] = (Button) findViewById(R.id.circle_one);
        circles[TWO] = (Button) findViewById(R.id.circle_two);
        circles[THREE] = (Button) findViewById(R.id.circle_three);
        circles[FOUR] = (Button) findViewById(R.id.circle_four);
        circles[FIVE] = (Button) findViewById(R.id.circle_five);
        circles[SIX] = (Button) findViewById(R.id.circle_six);

        flag = new boolean[MAX_SIZE];
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int circleNumber;
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                getXYCoordinate(event);
                radius = calculateRadius();
                calculateCenters();
                circleNumber = getCircleNumber();
                showPosition(circleNumber);
                changeButtonColor(circleNumber);
                break;
            case MotionEvent.ACTION_MOVE:
                getXYCoordinate(event);
                radius = calculateRadius();
                calculateCenters();
                circleNumber = getCircleNumber();
                showPosition(circleNumber);
                if (circleNumber != currentCircle) {
                    currentCircle = changeButtonColor(circleNumber);
                }
                break;
            case MotionEvent.ACTION_UP:
                output.setText(R.string.touch_circle);
                break;
        }
        return true;
    }

    private int changeButtonColor(int circleNumber) {
        if (circleNumber != MAX_SIZE) {
            if (flag[circleNumber] == RED) {
                circles[circleNumber].setBackgroundResource(R.drawable.circle_button);
                flag[circleNumber] = BLACK;
            } else {
                circles[circleNumber].setBackgroundResource(R.drawable.touched_circle);
                flag[circleNumber] = RED;
            }
        }
        return circleNumber;
    }

    private int getCircleNumber() {
        int number = MAX_SIZE;
        double distance = Double.MAX_VALUE;
        for (int i = 0; i < centerX.length; i++) {
            double tempDistance = calculateDistance(centerX[i], centerY[i]);
            if (distance > tempDistance && tempDistance <= radius) {
                distance = tempDistance;
                number = i;
                break;
            }
        }
        finalDistance = distance;
        return number;
    }

    private void showPosition(int currentCircle) {
        if (radius > finalDistance) {
            output.setText(INSIDE_CIRCLE + (currentCircle + 1));
        } else if (radius < finalDistance) {
            output.setText(OUTSIDE_CIRCLE);
        } else {
            output.setText(ON_THE_CIRCLE + (currentCircle + 1));
        }
    }

    private void getXYCoordinate(MotionEvent event) {
        xCoordinate = event.getX();
        yCoordinate = event.getY();
    }

    private double calculateDistance(double centerXCoordinate, double centerYCoordinate) {
        double xDifference = centerXCoordinate - xCoordinate;
        double yDifference = centerYCoordinate - yCoordinate;
        return Math.sqrt(Math.pow(xDifference, 2) + Math.pow(yDifference, 2));
    }

    private double calculateRadius() {
        return (circles[ONE].getWidth() / 2);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.next_button:
                Intent intent = new Intent(this, UserDefineCircles.class);
                startActivity(intent);
                break;
        }
    }
}
